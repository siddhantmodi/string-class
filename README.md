 Copyright (C) 2014 Siddhant Modi										   
                                                                          
 Description:                                                             
 Code created for CMPT 128 - Introduction To Computing Science II         
 final project. This project is a personalized re-creation of the         
 string class which is inherently available in C++. Constructors and	   
 destructors are created, along with operater definitions and member      
 functions. This is a simplified version of the string class and          
 functions can easily be added to it for greater functionality.           
                                                                          
 Available at:                                                            
 https://gitlab.com/siddhantmodi/string-class                             