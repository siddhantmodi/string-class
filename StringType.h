/* This file is a dependency for StringType.cpp*/

#include <iostream>
using namespace std;

class StringType //Course: CMPT128; Name: SIDDHANT MODI; Student ID: MODSD1203
{
public:
	//CONSTRUCTORS------------------------------------------------
	StringType();
	StringType(int);
	StringType(char*);
	StringType(const StringType&);
	~StringType();
	//FUNCTIONS---------------------------------------------------
	int length() const;
	int position(char) const;
	int substring(const StringType&) const; // Checks if the given string is a substring of the original string and returns the index of the starting point
	StringType upper(); // Returns string with all letters capitalized
	StringType reverse(); //Returns the reversed version of the string
	bool empty(); //Returns true if length is 0
	void swap(StringType&); //Swaps two strings
	//OPERATORS---------------------------------------------------
	friend ostream& operator << (ostream&, const StringType&);
	bool operator == (const StringType&) const;
	StringType& operator = (const StringType&);
	StringType operator + (const StringType&) const;
	StringType& operator += (const StringType&);
	StringType& operator += (char);
	char& operator [] (int); //Write
	const char& operator[] (int) const; //Read-only
private: //---------------------------------------------------------
	int max_length;
	char* value;
};