/****************************************************************************
* Copyright (C) 2014 Siddhant Modi										   *
*                                                                          *
* Description:                                                             *
* Code created for CMPT 128 - Introduction To Computing Science II         *
* final project. This project is a personalized re-creation of the         *
* string class which is inherently available in C++. Constructors and	   *
* destructors are created, along with operater definitions and member      *
* functions. This is a simplified version of the string class and          *
* functions can easily be added to it for greater functionality.           *
*                                                                          *
* Available at:                                                            *
* https://gitlab.com/siddhantmodi/string-class                             *
****************************************************************************/

#include "StringType.h"
#include <cstring>

int main()
{
	/* Code to test the functionality of the class */

	StringType string1("Sid"), string2("Modi"), string3("Siddhant Modi");
	cout << "String1: " << string1 << endl;
	cout << "String2: " << string2 << endl;

	string1.swap(string2);
	cout << "String1: " << string1 << endl;
	cout << "String2: " << string2 << endl;

	string1 = string1.reverse();
	string2 = string2.upper();
	cout << "String1: " << string1 << endl;
	cout << "String2: " << string2 << endl;

	StringType string4 = string2 + string1;
	cout << "String4: " << string4 << endl;

	string4 += "12345678";
	cout << "String4: " << string4 << endl;

	string4 += string3;
	cout << "String4: " << string4 << endl;

	int index = string4.substring("4567");
	if (index != -1)
	{
		cout << "4567" << " is a part of " << string4 << ". It begins at index " << index << ".\n";
	}
}

/********************************CLASS FUNCTION DEFINITIONS************************************/

/*******************************CONSTRUCTORS AND DESTRUCTORS***********************************/

StringType::StringType()
{
	max_length = 100;
	value = new char[max_length+1];
	value[0] = '\0';
}

StringType::StringType(int num)
{
	max_length = num;
	value = new char[max_length+1];
	value[0] = '\0';
}

StringType::StringType(char* myString)
{
	max_length = strlen(myString);
	value = new char[max_length+1];
	for (int i = 0; i < max_length; i++)
		value[i] = myString[i];
	value[max_length] = '\0';
}

StringType::StringType(const StringType& myString)
{
	max_length = strlen(myString.value);
	value = new char[max_length+1];
	for (int i = 0; i < max_length; i++)
		value[i] = myString.value[i];
	value[max_length] = '\0';
}

StringType::~StringType()
{
	delete [] value;
}

/*************************************FUNCTIONS****************************************/

int StringType::length() const
{
	return strlen(value);
}

StringType StringType::upper()
{
	StringType temp = *this;
	for (unsigned int i = 0; i < strlen(value); i++) 
		if (temp.value[i] >= 97 && temp.value[i] <= 122)
			temp.value[i] = temp.value[i] - 32;
	return temp;
}

int StringType::position(char char1) const
{
	for (int i = 0; i < max_length; i++) {
		if (value[i] == char1)
			return i;
	}
	return -1;
}

int StringType::substring(const StringType& myString) const
{
	int index(0), index2(0), locator(0);
	for (; index < length(); index++) {
		if (value[index] == myString.value[0]) {
			index2 = index; //The variable index needs to remain constant because it tracks the postion in the main array.
			for(int i = 0; i < myString.length();i++) 
				if (myString.value[i] == value[index2]) {
					index2++;
					locator = i + 1;
				}
		}
		if (locator == myString.length())
			return index;
	}
	
	return -1;
}

StringType StringType::reverse()
{
	char* temp = new char[length() + 1];
	for (int i(0), j(length()-1); i < length(); i++, j--) 
		temp[i] = value[j];
	temp[length()] = '\0';
	return StringType(temp);
}

bool StringType::empty()
{
	return (length() == 0);
}

void StringType::swap(StringType& myString)
{
	StringType temp = *this;
	*this = myString;
	myString = temp;
}

/************************************OPERATORS******************************************/

StringType& StringType::operator = (const StringType& myString)
{
	int new_length = strlen(myString.value);
	if (new_length > max_length) {
		delete [] value;
		max_length = new_length;
		value = new char[max_length+1];
	}

	for (int i = 0; i < new_length; i++)
		value[i] = myString.value[i];

	value[new_length] = '\0';
	return *this;
}


ostream& operator << (ostream& outs, const StringType& myString)
{
	int i = 0;
	do {
		outs << myString.value[i];
		i++;
	}
	while (myString.value[i] != '\0');

	return outs;
}

StringType StringType::operator + (const StringType& myString) const
{
	int new_length = length() + myString.length();
	char* string1 = new char[new_length+1];
	int i(0);
	for (; i < length(); i++) 
		string1[i] = value[i];
	for (int x = 0; x < myString.length(); x++, i++)
		string1[i] = myString.value[x];
	string1[new_length] = '\0';

	return StringType(string1);
}



bool StringType::operator == (const StringType& myString) const
{
	int counter(0);
	if (length() != myString.length())
		return false;
	else {
		for (int i = 0; i < length(); i++) 
			if (value[i] == myString.value[i])
				counter++;
		if (counter == length())
			return true;
		else
			return false;
	}
}

char& StringType::operator [] (int index)
{
	if (index > length())
	{
		cout << "Invalid index.";
	}
	else
		return value[index];
}

const char& StringType::operator [] (int index) const
{
	return value[index];
}

StringType& StringType::operator += (const StringType& myString)
{
	*this = *this + myString;
	return *this;
}

StringType& StringType::operator += (char char1)
{
	if (length()  < max_length) {
		value[length() + 1] = '\0';
		value[length()] = char1;
	}
	else {
		char* temp = new char[max_length+1];
		for (int i = 0; i < length(); i++)
			temp[i] = value[i];
		temp[max_length] = '\0';
		delete [] value;
		max_length++;
		value = new char[max_length+1];
		for (unsigned int i = 0; i < strlen(temp); i++)
			value[i] = temp[i];
		value[max_length-1] = char1;
		value[max_length] = '\0';
	}
	return *this;
}

/***************************************END***********************************************/



